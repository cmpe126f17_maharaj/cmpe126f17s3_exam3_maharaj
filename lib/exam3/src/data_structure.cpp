#include "data_structure.h"
#include <string>
using namespace std;

data_structure::data_structure() {
    // Default constructor: Generate an empty data structure
    for (int i=0; i < length; i++) {
        array[i] = 0;
    }
    for (int j=0; j < length; j++) {
        array2[j] = 0;
    }

}

data_structure::data_structure(string input_string) {
    /*// String constructor: Construct a data structure and store the input string into it
    for (int i=0; i < length; i++) {
        array[i] = input_string;
    }*/
    /*strArray[] = input_string.split;
    array[] = new int[strArray.length];

    for(int i=0; i < strArray.length; i++) {
        array[i] = integer.parseInt(strArray[i]);
    }*/
    /*strArray = input_string.split(",");
    int[] array = new int[strArray.length];
    for(int i = 0; i < strArray.length; i++) {
        array[i] = Integer.parseInt(strArray[i]);
    }*/
    /*string temp = ",";
    for (int i=0; i <length; i++) {
        input_string[temp]
    }*/
}

data_structure::~data_structure() {
    // Default Destructor: Deconstruct the data structure
    for (int k=0; k < length; k++) {
        array[k] = 0;
    }
    for (int l=0; l < length; l++) {
        array2[l] = 0;
    }
}

unsigned int data_structure::frequency(int input_character) {
    // Return the number of times the integer is in the data structure.
    int freq;
    for (int i=0; i < length; i++) {
        if (array[i] = input_character){
            freq ++;
        }
    }
    return freq;
}

int data_structure::most_frequent() {
    // Return the most frequent number in the data structure. If there is more than one, return the highest value
    /*int i = 0;
    int highfreq = array[i];
    int temp = array[i];
    for (i; i < length; i++) {
        array [i] = temp;
        if (array[i] = temp)
    }
    return temp;*/
    int k = 200;
    for (int i = 0; i< length; i++)
        array[array[i]%k] += k;

    // Find index of the maximum repeating element
    int max = array[0], result = 0;
    for (int i = 1; i < length; i++)
    {
        if (array[i] > max)
        {
            max = array[i];
            result = i;
        }
    }
    for (int i = 0; i< length; i++)
        array[i] = array[i]%k;

    return result;
}

int data_structure::least_frequent() {
    // Return the least frequent number in the data structure. If there is more than one, return the lowest value
    int k;
    for (int i = 0; i< length; i++)
        array[array[i]%k] += k;

    // Find index of the maximum repeating element
    int min = array[0], result = 0;
    for (int i = 1; i < length; i++)
    {
        if (array[i] < min)
        {
            min = array[i];
            result = i;
        }
    }
    for (int i = 0; i< length; i++)
          array[i] = array[i]%k;

    return result;
}

void data_structure::sort() {
    // Sort the data structure first by frequency, greatest to least and then by value, least to greatest.
    // Example: 1:3,42:4,17:3,11:1,46:1,3:2         sorted: 42:4,1:3,17:3,3:2,11:1,46:1
}

std::istream &operator>>(std::istream &stream, data_structure &structure) {
    // Stream in a string, empty the current structure and create a new structure with the new streamed in string.
    return stream;
}

std::ostream &operator<<(std::ostream &stream, const data_structure &structure) {
    // Stream out the data structure
    // Output in this format "<integer>:<frequency>,<integer>:<frequency>,<integer>:<frequency>"
    return stream;
}
